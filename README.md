# test1

TEST_1

## Description
Python script called 'HoeWarmIsHetInDelft.py' retrieves the current temperature from http://www.weerindelft.nl/ in Delft and prints it to standard output, rounded to degrees Celcius. Example expected output: 
```
18 degrees Celcius
```
## Usage
The code can be run by:
```
python HoeWarmIsHetInDelft.py
```
## Support
No support available.


## License
For open source projects, say how it is licensed.

## Project status
Development has stopped completely.