import time
from src.HoeWarmIsHetInDelft import WeerindelftTemp

def test_timestamp_type():
    t_float = time.time()
    assert type(WeerindelftTemp._get_timestamp(t_float=t_float)) is str
    
def test_timestamp_lenght():
    t_float = time.time()
    assert len(WeerindelftTemp._get_timestamp(t_float=t_float)) == 13

def test__get_url_format():
    t_float = 1644161805.7756104
    assert WeerindelftTemp._get_url(url="https://weerindelft.nl/clientraw.txt", t_float=t_float) == \
        "https://weerindelft.nl/clientraw.txt?1644161805775"

def test__get_not_existing_rawfile():
    t_obj = WeerindelftTemp()
    t_obj._url_str = WeerindelftTemp._get_url(url="https://weerindelft.nl/clientraw.txt1")
    try:
        t_obj._get_rawfile()
        assert False
    except ConnectionError:
        assert True

def test__get_current_temp_from_rawfile():
    t_obj = WeerindelftTemp()
    t_obj._raw_data = ['12345', '0.0', '0.0', '0.0', '10.5']
    assert t_obj.current_temp == '10'

def test__get_current_temp_from_rawfile_no_dot():
    t_obj = WeerindelftTemp()
    t_obj._raw_data = ['12345', '0.0', '0.0', '0.0', '10']
    assert t_obj.current_temp == '10'