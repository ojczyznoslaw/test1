"""
Module loading temperature from https://weerindelft.nl
"""

import time
import requests

class WeerindelftTemp():
    """ object class """
    __URL__ = "https://weerindelft.nl/clientraw.txt"
    __TEST_ID__ = 0
    __TEST_STR__ = '12345'
    __TEMP_ID__ = 4
    __DATARAW_LENGHT__ = 178

    def __init__(self):
        self._raw_data = None
        self._url_str = None

    @property
    def current_temp(self):
        """ gets current temp from raw data """
        if self._raw_data:
            return self._raw_data[self.__TEMP_ID__].split('.')[0]
        return "NA"

    @staticmethod
    def _get_timestamp(t_float=None) -> str:
        """ Calculate timestamp for the clientraw file """
        if not t_float:
            t_float = time.time()
        t_int = int(t_float)
        t_msec = int((t_float - t_int) * 1000)

        return f"{t_int:010}{t_msec:03}"

    @staticmethod
    def _get_url(url, t_float=None) -> str:
        """ creates url with timestamp """
        return f"{url}?{WeerindelftTemp._get_timestamp(t_float=t_float)}"

    def _get_rawfile(self) -> None:
        """ Function loading and checking the clientraw.txt file """
        # load file content
        response = requests.get(self._url_str)

        # check if response received
        if not response.ok:
            raise ConnectionError(f"Connection error, can't get to {self._url_str}. " \
                                  f"Response status code: {response.status_code}")

        # input string to list
        data = response.text.split()

        # check if raw file format is correct
        if data[self.__TEST_ID__] != self.__TEST_STR__ and len(data) == self.__DATARAW_LENGHT__:
            self._raw_data = None
            raise ValueError(f"DataRaw file is corrupted: {self._url_str}")

        self._raw_data = data

    def get_temp(self) -> None:
        """ Function returning the current time as integer """
        # create url for clientraw.txt file
        self._url_str = self._get_url(url=self.__URL__)
        try:
            # get raw file from srv
            self._get_rawfile()
        except ValueError as exp:
            print(f"ValueError: {exp}")
        except ConnectionError as exp:
            print(f"ConnectionError: {exp}")

    def __repr__(self) -> str:
        """ representation string"""
        return f"{self.current_temp} degrees Celcius"

if __name__ == '__main__':
    w_obj = WeerindelftTemp()
    w_obj.get_temp()
    print(w_obj)
